import React from "react";
import {
    shallow
} from "enzyme";
import Accordion from "./Accordion.js";

describe("<Accordion /> component", () => {
    const wrapper = shallow( < Accordion / > );
    
    it("should render", () => {
        expect(wrapper.find('.Accordion__panel')).toHaveLength(3);
    });

    it('should change classname', () => {
        const firstAccordionPanel = wrapper.find('.Accordion__panel').first();
         firstAccordionPanel.find('.Accordion__panel_head').simulate('click');

       expect(['Accordion__panel', 'Accordion__panel--active'].every(c => firstAccordionPanel.hasClass(c))).toBe(true);
    });

});