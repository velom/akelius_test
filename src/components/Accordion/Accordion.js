import React, { Component } from "react";
import "./Accordion.scss";

class Accordion extends Component {

  handlepanelClick = e => {
    if (e.target.classList.contains("Accordion__panel_head")) {
      e.currentTarget.classList.toggle("Accordion__panel--active");
    }
  };

  render() {
    return (
      <div className="Accordion">
        <div className="Accordion__panel" onClick={this.handlepanelClick}>
          <div className="Accordion__panel_head">Lorem, ipsum dolor.</div>
          <div className="Accordion__panel_content">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque nemo
            magni ad excepturi placeat fuga harum ullam laudantium soluta esse
            odio est nihil quo consectetur, minima nulla fugit, dolorum quis
            veniam illum temporibus repellendus vero. Expedita consequatur
            repellat, officiis deleniti tempore tempora similique unde, iusto
            cum pariatur corrupti nobis nostrum!
          </div>
        </div>
        <div className="Accordion__panel" onClick={this.handlepanelClick}>
          <div className="Accordion__panel_head">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Veniam,
            veritatis! Nobis ipsum qui rem commodi porro!
          </div>
          <div className="Accordion__panel_content">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque nemo
            magni ad excepturi placeat fuga harum ullam laudantium soluta esse
            odio est nihil quo consectetur, minima nulla fugit, dolorum quis
            veniam illum temporibus repellendus vero. Expedita consequatur
            repellat, officiis deleniti tempore tempora similique unde, iusto
            cum pariatur corrupti nobis nostrum!
          </div>
        </div>
        <div className="Accordion__panel" onClick={this.handlepanelClick}>
          <div className="Accordion__panel_head">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quae,
            nihil.
          </div>
          <div className="Accordion__panel_content">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque nemo
            magni ad excepturi placeat fuga harum ullam laudantium soluta esse
            odio est nihil quo consectetur, minima nulla fugit, dolorum quis
            veniam illum temporibus repellendus vero. Expedita consequatur
            repellat, officiis deleniti tempore tempora similique unde, iusto
            cum pariatur corrupti nobis nostrum!
          </div>
        </div>
      </div>
    );
  }
}

export default Accordion;
