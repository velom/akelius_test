import React from "react";
import { shallow } from "enzyme";
import Content from "./Content";

const buildSubject = ({ title } = {}) => {
  const props = { title }
  return shallow(<Content {...props} />);
};

describe("<Content /> component", () => {
  let subject;
  beforeEach(() => {
    subject = buildSubject({ title: 'Some Test component' });
  });

  it("should render", () => {
    expect(subject.containsMatchingElement(
      <h1>
        Some Test component
      </h1>)).toBe(true);
  });
});
