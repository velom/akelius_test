import React, { Component } from "react";
import Accordion from '../Accordion/Accordion';
import './Content.scss';

class Content extends Component {
  render() {
    return (
      <div className="Content">
        <h1>{this.props.title}</h1>
        <Accordion/>
      </div>
    );
  }
}

export default Content;
