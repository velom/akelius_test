import React, { Component } from "react";
import Sidebar from "../Sidebar/Sidebar";
import Content from "../Content/Content";
import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Sidebar />
        <Content title={'Hello world!!!'}/>
      </div>
    );
  }
}

export default App;
