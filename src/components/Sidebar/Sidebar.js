import React, { Component } from "react";
import "./Sidebar.scss";

const NAVIGATION = {
  dashboard: "/dashboard",
  basket: "/basket",
  inbox: "/inbox"
};

class Sidebar extends Component {
  renderNavigationItems = () => {
    const items = Object.keys(NAVIGATION).map((el, idx) => {
      return (
        <li key={idx} className="Navigation__item">
          <a href={NAVIGATION[el]} className="Navigation__link">
            {el}
          </a>
        </li>
      );
    });
    return items;
  };
  render() {
    return (
      <div className="Sidebar">
        <ul className="Navigation">
            {this.renderNavigationItems()}
        </ul>
      </div>
    );
  }
}

export default Sidebar;
