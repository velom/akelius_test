import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App/App";
import "./index.scss";

// import TestComponent from './components/TestComponent/TestComponent';
// const title = 'My Minimal React Webpack Babel Setup';

ReactDOM.render(<App />, document.getElementById("app"));

module.hot.accept();
